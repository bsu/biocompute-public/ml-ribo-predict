## Fastq Processing
___

Each replicates paired-end Fastq files wer joined using FLASh 1.2.11 on a computer operating Ubuntu 20.04.4. The following code was executed for each replicate pair:

```bash
flash -M 200 --allow-outies -t $(nproc) "file_1.fastq.gz" "file_2.fastq.gz"
```
with `file_1.fastq.gz` and `file_2.fastq.gz` replaced with the actual names of the input files to be joined.

This produces a series of output files that includes the joined sequence file needed to search for mutational variants. This file will be called `out.extendedFrags.fastq`

