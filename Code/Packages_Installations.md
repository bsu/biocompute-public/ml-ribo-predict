# Required Software and Packages

Fastq files were joined using [FLASh](https://ccb.jhu.edu/software/FLASH/). Sequences within the joined Fastq files were identified using [custom scripts]() written in [Julia](https://julialang.org). Activity data for identified sequences were predicted using [Pytorch](https://pytorch.org) and [Scipy](https://scipy.org) and custom [Python](https://www.python.org) scripts. Figures were created using [Matplotlib](https://matplotlib.org).

## Installed Software and Packages
---
### Shell - Used to Join Paired End Illumina Sequence Files
- `FLASh-1.2.11` 

**Note:** within the downloaded `FLASh-1.2.11-Linux-x86_64.tar.gz` is a Readme.md file that contains installation and basic usage instructions.


### Julia - Used to identify Sequences and Mutational Variants
- `Julia - 1.7.2`
- `BioSequences - 3.0.0`
- `CairoMakie - 0.7.5`
- `CSV - 0.10.4`
- `DataFrames - 1.3.3`
- `FASTX - v1.3.0`
- `Pickle - 0.3.0`
- `StatsBase - 0.33.16`

 
### Python - Used to Implement Pytorch and Scipy Machine Learning and Create Figures
- `Python - 3.9.7`
- `Matplotlib - 3.4.3`
- `Pytorch - 1.10.0`
- `Scipy - 1.7.1`
- `Torchtext - 0.11.0`

## Computer Specs
---
All code contained in this repository was executed on a `Dell Precision 5820` with:
- CPU - Intel i9 10980XE@3.0GHz with 18 cores 
- Memory - 64GB
- GPU - Nvidia Quadro RTX 4000
- NVIDIA CUDA Toolkit 11.5

> **Note:** CUDA GPU accelaration is not available unless the CUDA toolkit is installed.  