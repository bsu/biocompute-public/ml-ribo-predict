<div align="center">
<img src="hayden_lab.png">
</div>

# Predicting higher-order mutational effects in an RNA enzyme by machine learning of high-throughput experimental data

Ribozymes are RNA molecules that catalyze biochemical reactions. Self-cleaving ribozymes are a common naturally occurring class of ribozymes that catalyze site-specific cleavage of their own phosphodiester backbone. In addition to their natural functions, self-cleaving ribozymes have been used to engineer control of gene expression because they can be designed to alter RNA processing and stability. However, the rational design of ribozyme activity remains challenging, and many ribozyme-based systems are engineered or improved by random mutagenesis and selection (in vitro evolution). Improving a ribozyme-based system often requires several mutations to achieve the desired function, but extensive pairwise and higher-order epistasis prevent a simple prediction of the effect of multiple mutations that is needed for rational design. Recently, high-throughput sequencing-based approaches have produced data sets on the effects of numerous mutations in different ribozymes (RNA fitness landscapes). Here we used such high-throughput experimental data from variants of the CPEB3 self-cleaving ribozyme to train a predictive model through machine learning approaches. We trained models using either a random forest or long short-term memory (LSTM) recurrent neural network approach. We found that models trained on a comprehensive set of pairwise mutant data could predict active sequences at higher mutational distances, but the correlation between predicted and experimentally observed self-cleavage activity decreased with increasing mutational distance. Adding sequences with increasingly higher numbers of mutations to the training data improved the correlation at increasing mutational distances. Systematically reducing the size of the training data set suggests that a wide distribution of ribozyme activity may be the key to accurate predictions. Because the model predictions are based only on sequence and activity data, the results demonstrate that this machine learning approach allows readily obtainable experimental data to be used for RNA design efforts even for RNA molecules with unknown structures. The accurate prediction of RNA functions will enable a more comprehensive understanding of RNA fitness landscapes for studying evolution and for guiding RNA-based engineering efforts.


## Setup
- Instruction on installing [software and packages](Code/Packages_Installations.md) used to run this code.

## Code

The code links below will join paired-end Fastq files for the Mut_12 dataset and map the reads in those files to sequences. The Phylo dataset was acquired at [Bendixsen et al.](https://gitlab.com/devinbendixsen/cpeb3_phylo_fls). Resulting pickle (Mut_12) and csv (Phylo) files were used to create training and prediction sets (and reduced training sets). The sets were used to train predict using LSTM with Random Forest and Random Forest Models. Figures were produced using the prediction output.

- [Process Mut_12 dataset FastQ files](Code/_1_Join_reads/FLASh.md)
- [Map Mut_12 Reads to Mutations](Code/_2_Map_Mut12/reads/Fastq_Mut12_counts_CPEB3.ipynb)
- [Create Training and Prediction Sets](Code/_3_Create_sets/Make_csv.ipynb])
- [Create Reduced Training Sets](Code/_4_Reduced_sets/TrainSamples_CSV.ipynb)
- [Implement LSTM w\Random Forest](Code/_5_LSTM_RandomForest/LSTM_RandomForest.ipynb)
- [Implement Random Forest](Code/_6_RandomForest/RandomForest.ipynb)
- [Make Figures](Code/_7_Make_figures/)

## Data

### European Nucleotide Archive

CPEB3 Fastq files for the Mut_12 and Phylo datasets are available at the [European Nucleotide Archive](https://www.ebi.ac.uk/ena/browser/home)
- Mut_12: PRJEB51631
- Phylo: PRJEB36228

### Mut_12 Dataset

#### Included:

- `Data/CPEB3_Mut12.p` - An intermediary pickle file containing a dictionary of identifed Mut_12 sequences and their corresponding cleavage rates. 
- `Data/CPEB3_Mut12_cnts.p` - An intermediary pickle file containing a dictionary of identifed Mut_12 sequences and their corresponding counts.
- `Data/Mut12_counts` - a directory containing two files (`1_CPEB3_train.csv` and `2_CPEB3_train.csv`) used to organize single and double mutants within the Mut_12 Dataset. These are used to build the overall training sets.

### Phylogeny Dataset

#### Included:

- `Data/CPEB3_Phylo.csv` - a file contains the total phylogeny dataset including the genotype (column 1), 3 replicates of cleaved and uncleaved counts (columns 2-7), the cleavage rates for the three replicates (columns 8-10), and the average of the three rates (column 11). This data was obtained from [Bendixsen et al.](https://gitlab.com/devinbendixsen/cpeb3_phylo_fls)[^1].

- `Data/Phylo_counts` - a directory containing individual files (`phylo_mut*.csv`, with `*` ranging 1 to 13) organized by mutation order. These are used to build training and test sets.

### Train_Test_Dataset

#### Included: 

- `Data/Train_Test_Dataset` - contains all testing (predict) and training data used to implement the [LSTM w/Random Forest](Code/_5_LSTM_RandomForest/LSTM_RandomForest.ipynb) and [Random Forest](Code/_6_RandomForest/RandomForest.ipynb) models, and [make figures](Code/_7_Make_figures/).
    
    - `test` - files containing 20% of the Phylogeny dataset sequences and cleavage rates for a specified mutational order.
    - `train` - files containing 100% of the Mut_12 and 80% of the Phylogeny dataset for all mutational orders up to an including identified mutational order. For example, `6_train.csv` contains all of the Mut_12 dataset of single and double mutations and 80% of the Phylogeny dataset for 1 to 6 mutation variants.
    - `train_1` - files containing 1% of the total data obtained by randomly selecting from the `train` dataset.
    - `train_10` - files containing 10% of the total data obtained by randomly selecting from the `train` dataset.
    - `train_20` - files containing 20% of the total data obtained by randomly selecting from the `train` dataset.
    - `train_40` - files containing 40% of the total data obtained by randomly selecting from the `train` dataset.
    - `train_60` - files containing 60% of the total data obtained by randomly selecting from the `train` dataset.    

## Figures

#### Manuscript Figures
Figures were produced using the predictions saved in [Results](/Results/)
- [Figure 1](Figures/Figure%201%20-%20v5.jpg)
- [Figure 2](Figures/Figure%202%20with%20panel%20letters.jpg)
- [Figure 3](Figures/Figure%203%20with%20panel%20letters.jpg\)
- [Figure 4](Figures/Figure%204%20with%20panel%20letters.jpg)

#### Supplement Figures
Supplement figures can be found in [Figures/Supplement](Figures/Supplement/)

## Results
Contains predictions made by the [LSTM w/Random Forest](Results/LSTM_RF/) and the [Random Forest](Results/RF_80/). Additional results for reduced datasets can be found within the [Results](/Results/) directory.  


## Contributors
- Jim Beck<sup>1,†</sup>  
- Jessica M. Roberts<sup>2,†</sup>   
- Joey Kitzhaber<sup>3</sup>  
- Ashlyn Trapp<sup>4</sup>   
- Edoardo Serra<sup>1</sup>   
- Francesca Spezzano<sup>1</sup>  
- Eric J. Hayden <sup>2,3,*</sup>  

<font size="2">
1 Computing PhD Program, Boise State University, Boise, ID, USA<br>
2 Biomolecular Sciences Graduate Programs, Boise State University, Boise, ID, USA<br>
3 Department of Computer Science, Boise State University, Boise, ID, USA<br>
4 Department of Biological Sciences, Boise State University, Boise, ID, USA<br> 
† Authors contributed equally to this work and share first authorship<br>
* Correspondence: Eric J. Hayden erichayden@boisestate.edu<br>
</font>


[^1]: Bendixsen, D. P., Pollock, T. B., Peri, G., & Hayden, E. J. (2021). Experimental resurrection of ancestral mammalian CPEB3 ribozymes reveals deep functional conservation. Molecular Biology and Evolution, 38(7), 2843-2853.

